package ru.smbatyan.discount_search_service;

import java.net.http.HttpClient;
import java.time.Duration;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

public class CrptApi {

    private final Semaphore semaphore;
    private final Long counterResetTimeInSeconds;
    private final Integer requestLimit;
    private final ApiOfAnHonestSign apiOfAnHonestSign = new ApiOfAnHonestSign();

    public CrptApi(Duration duration, Integer requestLimit) {
        semaphore = new Semaphore(requestLimit);
        this.requestLimit = requestLimit;
        counterResetTimeInSeconds = duration.getSeconds();
        resetSemaphore();
    }


    private void resetSemaphore() {
        new Thread(() -> {
            while (true) {
                LockSupport.parkNanos(TimeUnit.SECONDS.toNanos(counterResetTimeInSeconds));
                semaphore.release(requestLimit);
            }
        }).start();
    }


    public void toPutIntoCirculationGoodsProducedInTheRussianFederation(DocumentForm documentForm) {
        try {
            semaphore.acquire();
            apiOfAnHonestSign.toPutIntoCirculationGoodsProducedInTheRussianFederation(documentForm, "blabla");

        } catch (InterruptedException e) {
            throw new IllegalArgumentException(e);
        }
    }
}


class DocumentForm {
    private final String docId;
    //...
    //...

    public DocumentForm(String docId) {
        this.docId = docId;
        //...
        //...
    }

    public String getDocId() {
        return docId;
    }
    //...
    //...
}


class ApiOfAnHonestSign {

    private static final String URL_DOCUMENTS_CREATE = "https://ismp.crpt.ru/api/v3/lk/documents/create";


    public void toPutIntoCirculationGoodsProducedInTheRussianFederation(DocumentForm documentForm, String signature) {
        HttpClient httpClient;//...
        System.out.println("Test");
    }
}
